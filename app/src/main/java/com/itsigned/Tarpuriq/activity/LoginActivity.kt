package com.itsigned.Tarpuriq.activity

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.itsigned.Tarpuriq.R
import com.itsigned.Tarpuriq.RafiServiceWrapper
import com.itsigned.Tarpuriq.bean.User
import com.itsigned.Tarpuriq.fragment.REQUEST_PERMISSION_CODE
import com.itsigned.Tarpuriq.helper.FileHelper
import com.itsigned.Tarpuriq.helper.goToActivity
import com.itsigned.Tarpuriq.helper.hasErrorEditTextEmpty
import com.itsigned.Tarpuriq.helper.showError
import com.itsigned.Tarpuriq.mapper.GeneralMapper
import com.itsigned.Tarpuriq.model.LangRequestDto
import com.itsigned.Tarpuriq.model.LoginRequestDto
import com.itsigned.Tarpuriq.util.Util
import com.itsigned.Tarpuriq.util.session.SessionManager
import kotlinx.android.synthetic.main.activity_login.*

private const val REQUEST_SIGNUP = 0
class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        if (SessionManager.getInstance(this).isLogged) goToActivity()
        configureActionButton()
        verifiyPermisied()
    }


    private fun verifiyPermisied() {
        if (enablePermise(Manifest.permission.RECORD_AUDIO)|| enablePermise(Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                enablePermise(Manifest.permission.CAPTURE_AUDIO_OUTPUT) ) {
            if (Build.VERSION.SDK_INT >= 23) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAPTURE_AUDIO_OUTPUT), REQUEST_PERMISSION_CODE)
            }
        }
    }

    private fun enablePermise(permission:String):Boolean{
        return ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED
    }

    /**
     * Metodo con las configuraciones iniciales de los botones
     */
    private fun configureActionButton() {
        btnRegistro.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivityForResult(intent, REQUEST_SIGNUP)
        }
        btnLogin.setOnClickListener { login() }
    }


    /**
     * Metodo para autentificar al usuario
     */
    private fun verifyLoginExtern() {
        Log.d("verify extern","verify extern")
        val progress = Util.createProgressDialog(this, "Cargando")
        progress.show()
        RafiServiceWrapper.loginUser(this,
                LoginRequestDto(email = etEmail.text.toString(), password = etPass.text.toString()),
                { loginUser ->
                    updateListWord(progress,GeneralMapper.loginUserDtoDtoToUser(loginUser))
                },
                { error ->
                    progress.dismiss()
                    Toast.makeText(baseContext, error, Toast.LENGTH_LONG).show()
                })

    }

    private fun updateListWord(progress:ProgressDialog,newUser:User){
        RafiServiceWrapper.getTextPromp(this, LangRequestDto(newUser.idLanguage),
                { list->
                    val gson = Gson()
                    val jsonString = gson.toJson(list)
                    Log.d("finishList",jsonString)
                    FileHelper.writeToFile(this,jsonString)
                    progress.dismiss()
                    SessionManager.getInstance(baseContext).createUserSession(newUser)
                    goToActivity()
        },{
            error->Toast.makeText(baseContext, error, Toast.LENGTH_LONG).show()

        })
    }


    /**
     * Sobreescritura del metodo onActivityResult
     * @param requestCode codigo del Request del activity
     * @param resultCode codigo de resultado del activity
     * @param data extraData con información de la accion
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d("result ", "result $resultCode request $requestCode")
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == Activity.RESULT_OK) {
                setResult(Activity.RESULT_OK)
                this.finish()
            }
        }
    }

    /**
     * Metodo para validar los datos del login
     */
    private fun login() {
        if (hasErrorEditTextEmpty(etEmail, R.string.registro_message_error_ingrese_correo_electronico)) return
        if (hasErrorEditTextEmpty(etPass, R.string.registro_message_error_ingrese_contrasena_usuario)) return
        if (!Util.validarCorreo(etEmail.text.toString())) {
            showError(etEmail, getString(R.string.registro_message_error_correo_electronico_invalido))
            return
        }
        verifyLoginExtern()
    }


}
