package com.itsigned.Tarpuriq.activity

import android.app.Activity
import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.gson.Gson
import com.itsigned.Tarpuriq.R
import com.itsigned.Tarpuriq.RafiServiceWrapper
import com.itsigned.Tarpuriq.bean.Ubigeo
import com.itsigned.Tarpuriq.bean.User
import com.itsigned.Tarpuriq.database.DataBaseService
import com.itsigned.Tarpuriq.helper.FileHelper
import com.itsigned.Tarpuriq.helper.goToActivity
import com.itsigned.Tarpuriq.helper.hasErrorEditTextEmpty
import com.itsigned.Tarpuriq.mapper.GeneralMapper
import com.itsigned.Tarpuriq.model.LangRequestDto
import com.itsigned.Tarpuriq.model.Language
import com.itsigned.Tarpuriq.model.LoginRequestDto
import com.itsigned.Tarpuriq.util.Util
import com.itsigned.Tarpuriq.util.session.SessionManager
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.etEmail

private const val SEARCH_DEPARTMENT=1
private const val SEARCH_PROVINCIA=2
private const val SEARCH_DISTRITO=3
class RegisterActivity : AppCompatActivity() {

    private var listaDepartamento: ArrayList<Ubigeo>? = null
    private var listLanguage:ArrayList<Language>?=null
    private var departamento: String? = null
    private var provincia: String? = null
    private var distrito: String? = null
    private var ubigeoSelected: Ubigeo? = null
    private var idDialect:Int?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        setupToolbar()

        val email=intent.getStringExtra("email")
        if(email != null){
            etEmail.setText(email)
            etEmail.isEnabled=false
        }
        wrapperQueryDataBase(SEARCH_DEPARTMENT) { x->listaDepartamento=x }
        configureSpinner()
    }

    /**
     * Método para llenar los spinner iniciales y configurar sus eventos
     */
    private fun configureSpinner(){
        spDepartamento.onItemSelectedListener =   getListenerSpinner { position->spinnerItemSelectedDepartamento(position) }
        spProvincia.onItemSelectedListener = getListenerSpinner { position->spinnerItemSelectProvincia(position) }
        spDistrito.onItemSelectedListener = getListenerSpinner { position->spinnerItemSelectDistrito(position) }
        val departamentoAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, listaDepartamento!!.toMutableList())
        spDepartamento.adapter = departamentoAdapter
        fillListLanguage()
    }

    /**
     * Método que invoca un webservice para obtener los dialectos para el registro
     */
    private fun fillListLanguage(){
        val progress = Util.createProgressDialog(this, "Cargando")
        progress.show()
        RafiServiceWrapper.getLanguage(this, {
            list->
            this.listLanguage= ArrayList(list)
            val langueageAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item,list.toMutableList())
            spDialecto.adapter=langueageAdapter
            idDialect=list[0].language_id
            spDialecto.onItemSelectedListener = getListenerSpinner { position->this.idDialect= listLanguage!![position].language_id}
            progress.dismiss()
        },{error->
            progress.dismiss()
            Toast.makeText(this, error, Toast.LENGTH_LONG).show()
        })
    }

    /**
     * Metodo para obtener los ubigeos de la base de datos interna
     * @param typeUbigeo=Tipo de ubigeo
     * @param idDepartamento=Id del departamento a obtener
     * @param idProvincia Id de la provincia
     * @param getUbigeo lambda con donde se coloca la acción a realizar cuando se obtuvo exito en la consulta
     */
    private fun wrapperQueryDataBase(typeUbigeo:Int,idDepartamento:Int=0,idProvincia:Int=0,getUbigeo: (listUbigeo:  ArrayList<Ubigeo>? ) -> Unit){
        val dataBaseService = DataBaseService.getInstance(this)
        try {
            dataBaseService.listDepartamento
            val listUbigeo=when(typeUbigeo){
                SEARCH_DEPARTMENT -> dataBaseService.listDepartamento
                SEARCH_PROVINCIA-> dataBaseService.getListProvincia(idDepartamento)
                SEARCH_DISTRITO->  dataBaseService.getListDistrito(idDepartamento, idProvincia)
                else->throw java.lang.Exception()
            }
            getUbigeo(listUbigeo )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Metodo que indica la acción a realizar cuando se selecciona un departamento
     * @param position posicion de la lista del departamento seleccionado
     */
    private fun spinnerItemSelectedDepartamento(position: Int) {
        val ubigeo = spDepartamento.getItemAtPosition(position) as Ubigeo
        departamento = ubigeo.nombre
        var listaProvincia: ArrayList<Ubigeo> = ArrayList()
        wrapperQueryDataBase(SEARCH_PROVINCIA,ubigeo.idDepartamento){ x->  listaProvincia =x!! }
        val provinciaAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, listaProvincia)
        spProvincia.adapter = provinciaAdapter
    }

    /**
     * Metodo que indica la acción a realizar cuando se selecciona una provincia
     * @param position posicion de la lista de la provincia seleccionada
     */
    private fun spinnerItemSelectProvincia(position: Int) {
        val ubigeo = spProvincia.getItemAtPosition(position) as Ubigeo
        provincia = ubigeo.nombre
        var listaDistrito: ArrayList<Ubigeo> = ArrayList()
        wrapperQueryDataBase(SEARCH_DISTRITO,ubigeo.idDepartamento,ubigeo.idProvincia){ x->  listaDistrito =x!! }
        val distritoAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, listaDistrito)
        spDistrito.adapter = distritoAdapter
        btnSignup.setOnClickListener { registrarUser() }
    }

    /**
     * Metodo que indica la acción a realizar cuando se selecciona una provincia
     * @param position posicion de la lista de la provincia seleccionada
     */
    private fun spinnerItemSelectDistrito(position: Int) {
        val ubigeo = spProvincia.getItemAtPosition(position) as Ubigeo
        distrito = departamento + "/" + provincia + "/" + ubigeo.nombre
        ubigeoSelected = ubigeo
    }

    private fun setupToolbar() {
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        val ab = supportActionBar
        ab!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                this.finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }



    /**
     * Metodo que valida los datos del registro de usuario, si son correctos invoca un metodo para registrar al usuario,
     * caso contrario marca el error en la vista
     */
    private fun registrarUser() {
        if (hasErrorEditTextEmpty(etEmail,R.string.registro_message_error_ingrese_correo_electronico)) return
        if (hasErrorEditTextEmpty(etPassword,R.string.registro_message_error_ingrese_contrasena_usuario)) return
        if (hasErrorEditTextEmpty(etPaterno,R.string.registro_message_error_ingrese_apellido_paterno)) return
        if (hasErrorEditTextEmpty(etName,R.string.registro_message_error_ingrese_nombres)) return
        if (hasErrorEditTextEmpty(etDni,R.string.registro_message_error_formato_dni)) return
        if (hasErrorEditTextEmpty(etTelefono,R.string.registro_message_error_formato_phone)) return
        if (!Util.validarCorreo(etEmail.text.toString())) {
            showError(etEmail, getString(R.string.registro_message_error_correo_electronico_invalido))
            return
        }
        registrar()
    }


    private fun showError(editText: EditText, message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        editText.error = message
        editText.isFocusable = true
        editText.requestFocus()
    }

    /**
     * Metodo para registrar el usuario mediante un Webservice
     */
    private fun registrar() {
        val usuario = User()
        usuario.firstName = etName.text.toString()
        usuario.lastName = etPaterno.text.toString()
        usuario.email = etEmail.text.toString()
        usuario.password = etPassword.text.toString()
        usuario.phone = etTelefono.text.toString()
        usuario.dni = etDni.text.toString()
        usuario.codeDepartamento = ubigeoSelected?.idDepartamento
        usuario.codeProvincia = ubigeoSelected?.idProvincia
        usuario.codeDistrito = ubigeoSelected?.idDistrito
        usuario.avance = 0
        usuario.idLanguage=idDialect
        registerByServiceWeb(usuario)
    }

    /**
     * Metodo para crear una sesión del usuario y llevarlo a la vista principal
     */
    private fun createSession(user: User){
        Log.d("create user","create user")
        SessionManager.getInstance(baseContext).createUserSession(user)
        user.userExternId = 0
        setResult(Activity.RESULT_OK, null)
        finish()
        goToActivity()
    }

    /**
     * Metodo que invoca el webService para registrar el usuario
     * @param user objeto del tipo Usuario con la información del usuario
     */
    private fun registerByServiceWeb(user: User) {
        val progress = Util.createProgressDialog(this, "Cargando")
        progress.show()
        RafiServiceWrapper.registerUser(this,GeneralMapper.userToRegisterUserDto(user),{
            _->
            progress.dismiss()
            verifyLoginExtern(user)
        },{x->
            progress.dismiss()
            Toast.makeText(baseContext, x, Toast.LENGTH_LONG).show()
        })
    }

    /**
     * Metodo para autenticar un usuario
     * @param user datos del usuario a autentificar
     */
    private fun verifyLoginExtern(user: User) {
        val progress = Util.createProgressDialog(this, "Cargando")
        progress.show()
        RafiServiceWrapper.loginUser(this,
                LoginRequestDto(email = user.email, password = user.password),
                { loginUser ->
                    updateListWord(progress,GeneralMapper.loginUserDtoDtoToUser(loginUser))
                },
                { error ->
                    progress.dismiss()
                    Toast.makeText(baseContext, error, Toast.LENGTH_LONG).show()
                })

    }

    private fun updateListWord(progress: ProgressDialog, newUser:User){
        RafiServiceWrapper.getTextPromp(this, LangRequestDto(newUser.idLanguage),
                { list->
                    val gson = Gson()
                    val jsonString = gson.toJson(list)
                    Log.d("finishList",jsonString)
                    FileHelper.writeToFile(this,jsonString)
                    progress.dismiss()
                    createSession(newUser)
                },{
            error->Toast.makeText(baseContext, error, Toast.LENGTH_LONG).show()

        })
    }



    private fun getListenerSpinner(itemSelect: (position: Int) -> Unit): AdapterView.OnItemSelectedListener {
        return object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                itemSelect(position)
            }
        }
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }
}

