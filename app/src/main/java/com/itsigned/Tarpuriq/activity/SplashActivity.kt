package com.itsigned.Tarpuriq.activity

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import com.itsigned.Tarpuriq.R
import com.itsigned.Tarpuriq.util.session.SessionManager
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import java.util.*


class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_splash)

        AppCenter.start(application, "925ab580-a58f-4abe-9112-bac71e003c7b",
                Analytics::class.java, Crashes::class.java)


        val task: TimerTask = object : TimerTask() {
            override fun run() {
                finish()
                val classActivityToInit =
                        if (SessionManager.getInstance(this@SplashActivity).isLogged)
                            MainActivity::class.java else LoginActivity::class.java
                val mainIntent = Intent().setClass(
                        this@SplashActivity, classActivityToInit)
                startActivity(mainIntent)
            }
        }

        val timer = Timer()
        timer.schedule(task, 4000)
    }

}