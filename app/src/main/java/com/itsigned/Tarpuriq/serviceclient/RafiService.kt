package com.itsigned.Tarpuriq


import com.itsigned.Tarpuriq.model.*
import com.itsigned.Tarpuriq.util.HttpRequestConstants.Companion.URL_BASE
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit


/**
 * clase con la interface del Retrofit
 */
interface RafiService {

    @POST("/account_app")
    fun register(@Header("Content-Type")  contentType:String, @Body body: RegisterUserDto): Observable<RegisterUserDto>

    @POST("/login_app")
    fun loginApp(@Header("Content-Type")  contentType:String, @Body body: LoginRequestDto): Observable<LoginUserDto>

    @POST("/email_app")
    fun userByMail(@Header("Content-Type")  contentType:String, @Body body: UserByEmailRequestDto): Observable<LoginUserDto>

    @POST("/count_text")
    fun getTextPromp(@Header("Content-Type")  contentType:String,@Body body: LangRequestDto): Observable<TextPrompDto>

    @POST("/lang")
    fun getLanguage(@Header("Content-Type")  contentType:String): Observable<ResponseLangDto>

    @Multipart
    @POST("/upload_app")
    fun uploadAudio(@Part file: MultipartBody.Part): Observable<Any>



    companion object Factory {


        fun create(): RafiService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(URL_BASE)
                .build()
            return retrofit.create(RafiService::class.java)
        }

        fun createForFile(): RafiService {
            val client = OkHttpClient.Builder()
                .connectTimeout(14, TimeUnit.MINUTES).
            writeTimeout(14, TimeUnit.MINUTES)
                .readTimeout(14, TimeUnit.MINUTES).build()

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(URL_BASE)
                .client(client)
                .build()
            return retrofit.create(RafiService::class.java)
        }
    }
}