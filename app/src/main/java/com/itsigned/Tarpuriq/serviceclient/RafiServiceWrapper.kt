package com.itsigned.Tarpuriq

import android.content.Context
import android.util.Log
import com.itsigned.Tarpuriq.model.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import okhttp3.RequestBody

const val TAG = "RafiServiceWrapper"
const val CONTENT_TYPE_JSON="application/json"

/**
 * Clase con metodos para invocar webService
 */
class RafiServiceWrapper {
    companion object {

        /**
         * Metodo para invocar al servicio de registro de usuario
         * @param context contexto de la aplicacion
         * @param body cuerpo de la peticion, contiene los datos de registro
         * @param onSuccess metodo para invocar si la peticion al servicio es exitosa
         * @param onError metodo para invocar si la peticion es erronea
         */
        fun registerUser(context: Context, body: RegisterUserDto, onSuccess: (success: RegisterUserDto) -> Unit, onError: (error: String) -> Unit) {
            val apiService = RafiService.create()
            Log.d(TAG,"execute service register whith")
            Log.d(TAG,body.toString())
            apiService.register(CONTENT_TYPE_JSON,body)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            { result -> onSuccess(result) },
                            { error ->
                                Log.d(TAG,"error register user")
                                error.printStackTrace()
                                onError(context.getString(R.string.generic_error)) }
                    )
        }

        /**
         * Metodo para invocar al servicio de obtener frases segun el dialecto
         * @param context contexto de la aplicacion
         * @param body cuerpo de la peticion, contiene el dialecto
         * @param onSuccess metodo para invocar si la peticion al servicio es exitosa
         * @param onError metodo para invocar si la peticion es erronea
         */
        fun getTextPromp(context: Context, body: LangRequestDto, onSuccess: (success: TextPrompDto) -> Unit, onError: (error: String) -> Unit) {
            val apiService = RafiService.create()
            Log.d(TAG,"execute service get TextPromp")
            Log.d(TAG,body.toString())
            apiService.getTextPromp(CONTENT_TYPE_JSON,body)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            { result -> onSuccess(result) },
                            { error ->
                                Log.d(TAG,"error getTextPromp")
                                error.printStackTrace()
                                onError(context.getString(R.string.generic_error)) }
                    )
        }

        /**
         * Metodo para obtener los dialectos del servicio web
         * @param context contexto de la aplicacion
         * @param onSuccess metodo para invocar si la peticion al servicio es exitosa
         * @param onError metodo para invocar si la peticion es erronea
         */
        fun getLanguage(context: Context, onSuccess: (success: List<Language>) -> Unit, onError: (error: String) -> Unit) {
            val apiService = RafiService.create()
            Log.d(TAG,"execute service getLanguage")
            apiService.getLanguage(CONTENT_TYPE_JSON)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            { result -> onSuccess(result.language) },
                            { error ->
                                Log.d(TAG,"error language")
                                error.printStackTrace()
                                onError(context.getString(R.string.generic_error)) }
                    )
        }

        /**
         * Metodo para invocar al servicio de logueo de usuario
         * @param context contexto de la aplicacion
         * @param body cuerpo de la peticion, contiene el usuario y password para el logueo
         * @param onSuccess metodo para invocar si la peticion al servicio es exitosa
         * @param onError metodo para invocar si la peticion es erronea
         */
        fun loginUser(context: Context, body: LoginRequestDto, onSuccess: (success: LoginUserDto) -> Unit, onError: (error: String) -> Unit) {
            Log.d(TAG,"execute service loginUser whith")
            Log.d(TAG,body.toString())
            val apiService = RafiService.create()
            apiService.loginApp(CONTENT_TYPE_JSON,body)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            { result ->
                                onSuccess(result)

                            },
                            { error ->
                                Log.d(TAG,"error loginUser")
                                error.printStackTrace()
                                onError(context.getString(R.string.generic_error)) }
                    )
        }


        /**
         * Metodo para invocar al servicio de subir un arhivo de audio
         * @param fileName nombre del archivo de audio
         * @param requestBody cuerpo de la peticion, contiene los datos del archivo de audio
         * @param context contexto de la aplicación
         * @param onSuccess metodo para invocar si la peticion al servicio es exitosa
         * @param onError metodo para invocar si la peticion es erronea
         */
        fun uploadAudio(fileName:String, requestBody: RequestBody, context: Context,
                               onSuccess: () -> Unit, onError: (error: String?) -> Unit) {
            Log.d(TAG,"execute service uploadAudio whith")
            Log.d(TAG,requestBody.toString())
            val part = MultipartBody.Part.createFormData("files", fileName, requestBody)
            val apiService = RafiService.create()
            apiService.uploadAudio(part)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({  onSuccess()
                    }, { error ->
                        Log.d(TAG,"error upload Audio")
                        error.printStackTrace()
                        onError(context.getString(R.string.generic_error))
                    }
                    )
        }
    }
}