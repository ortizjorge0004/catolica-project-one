package com.itsigned.Tarpuriq.fragment

import android.Manifest
import android.Manifest.permission.RECORD_AUDIO
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.pm.PackageManager
import android.graphics.drawable.AnimationDrawable
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.itsigned.Tarpuriq.R
import com.itsigned.Tarpuriq.RafiServiceWrapper
import com.itsigned.Tarpuriq.bean.User
import com.itsigned.Tarpuriq.helper.FileHelper
import com.itsigned.Tarpuriq.model.LangRequestDto
import com.itsigned.Tarpuriq.model.TextAudio
import com.itsigned.Tarpuriq.model.TextPrompDto
import com.itsigned.Tarpuriq.player.MediaPlayerHolder
import com.itsigned.Tarpuriq.player.MediaRecordHolder
import com.itsigned.Tarpuriq.util.Util
import com.itsigned.Tarpuriq.util.session.SessionManager
import kotlinx.android.synthetic.main.fragment_record_audio.*
import kotlinx.android.synthetic.main.fragment_record_audio.view.*
import okhttp3.RequestBody
import java.io.File

const val REQUEST_PERMISSION_CODE = 1
class RecordAudioFragment : Fragment(), MediaPlayerHolder.EventMediaPlayer ,MediaRecordHolder.EventMediaRecordHolder{
    override fun reinitAudio() {
    }

    private var mPlayerAdapter: MediaPlayerHolder? = null
    private var mediaPlayerHolderForRecord: MediaPlayerHolder? = null
    private lateinit var listTextAudio:List<TextAudio>
    private var mediaRecordHolder: MediaRecordHolder? = null
    private var mUserIsSeeking = false
    private var mUserRecordIsSeeking = false
    private lateinit var seekbarExample:SeekBar
    private lateinit var seekbarRecord:SeekBar
    private var index=0
    private  var totalAudio:Int=0
    private lateinit var fileName:String
    private var frameAnimation: AnimationDrawable? = null
    private lateinit var user: User
    private lateinit var lastAudioRecord:String

    /**
     * Sobreescritura del metodo onCreateView
     * @param inflater clase para inflar el layout
     * @param container contenedor de la vista
     * @param savedInstanceState bundle con información del activity previo
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_record_audio, container, false)
        user=SessionManager.getInstance(context!!).userLogged!!
        fileName=Util.getFileName()
        totalAudio=Util.getTotalAudio(fileName)
        initializeSeekbarRecord(view)
        initializePlaybackController()
        verifiyPermisied()
        getListTextAudio()
        if(user.avance==null)index=1 else index=user.avance

        view.textQuechua.text = listTextAudio[index].text
        initButton(view)
        view.audioRecord.visibility=View.GONE
        return view
    }

    private fun getListTextAudio(){
        val jsonString=FileHelper.readFromFile(context!!)
        val gson=Gson()
        listTextAudio=gson.fromJson(jsonString, TextPrompDto::class.java).text_prompts
    }


    private fun initButton(view:View){
        view.btnNextAudio.setOnClickListener {nextAudio();updateServerRemote()}
        view.buttonNextAudio.setOnClickListener {nextAudioWhitoutSend() }

        view.ibPlayRecord.setOnClickListener { showHidenButtonControlMediaRecord(view, true);mediaPlayerHolderForRecord?.play() }
        view.ibPauseRecord.setOnClickListener { showHidenButtonControlMediaRecord(view, false); mediaPlayerHolderForRecord?.pause() }
        view.ivCloseRecord.setOnClickListener {dialogClose()}
        view.btnRecord.setOnClickListener {recordAudio()}
        view.btnPauseRecord.setOnClickListener {mediaRecordHolder?.stopRecord();btnRecord.isEnabled=true}
    }


    private fun showHidenButtonControlMediaRecord(view: View, isPlaying: Boolean) {
        view.ibPauseRecord.visibility = if (isPlaying) View.VISIBLE else View.GONE
        view.ibPlayRecord.visibility = if (isPlaying) View.GONE else View.VISIBLE
    }


    /**
     * Metodo para mover la posicion de la barra Seek
     * @param pos posiciion de la barra seek a donde se va a mover [ 0 - 100 ]
     */
    override fun changePositionSeek(pos: Int) {
        if (mUserIsSeeking) return
        seekbarExample.progress = pos
    }


    private fun initializePlaybackController() {
        val mMediaPlayerHolder = MediaPlayerHolder(this.activity, this)
        mPlayerAdapter = mMediaPlayerHolder
        mediaRecordHolder=MediaRecordHolder(this)
        initMediaPlayerForRecord()

    }




    private fun initMediaPlayerForRecord(){
        mediaPlayerHolderForRecord= MediaPlayerHolder(this.activity!!, object : MediaPlayerHolder.EventMediaPlayer {
            override fun reinitAudio() { showHidenButtonControlMediaRecord(view!!,false) }

            override fun changePositionSeek(pos: Int) { if (mUserRecordIsSeeking) return;seekbarRecord.progress = pos }

            override fun onPositionChanged(positon: Int) {if (!mUserRecordIsSeeking) seekbarRecord.progress = positon }

            override fun onDurationChanged(duration: Int) { seekbarAudioRecord.max = duration }
        }
        )
    }

    /**
     * Metodo para inicializar los Seekbar de reproduccion de audio de grabación
     * @param view view con información del layout
     */
    private fun initializeSeekbarRecord(view:View) {
        seekbarRecord=view.seekbarAudioRecord
        view.seekbarAudioRecord.setOnSeekBarChangeListener(
                object : SeekBar.OnSeekBarChangeListener {
                    var userSelectedPosition = 0

                    override fun onStartTrackingTouch(seekBar: SeekBar) {
                        mUserRecordIsSeeking = true
                    }

                    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                        if (fromUser)
                            userSelectedPosition = progress
                    }

                    override fun onStopTrackingTouch(seekBar: SeekBar) {
                        mUserRecordIsSeeking = false
                        mediaPlayerHolderForRecord?.seekTo(userSelectedPosition)
                    }
                })
    }


    /**
     * Metodo que se activa cada vez que se cambia la posicion del SeekBar
     * @param positon nueva posición del seekbar
     */
    override fun onPositionChanged(positon: Int) {
        if (!mUserIsSeeking) seekbarExample.progress = positon
        if (view!=null) showHidenButtonControlMediaRecord(view!!,false)
    }

    override fun onDurationChanged(duration: Int) {
        seekbarExample.max = duration
    }

    /**
     * Metodo para iniciar la grabación de un adio
     */
    private fun recordAudio(){
        btnRecord.isEnabled=false
        btnNextAudio.isEnabled=false
        initAnimation()
        if (checkPermission())mediaRecordHolder?.initRecord(SessionManager.getInstance(activity)
                .userLogged.dni.toString(),listTextAudio[index].id
                ,context!!) else verifiyPermisied()
    }

    /**
     * Metodo para mostrar un cuadro de confirmación para eliminar el audio grabado
     */
    private fun dialogClose(){
        val builder1 =  AlertDialog.Builder(this.context!!)
        builder1.setMessage("¿Desea eliminar la grabación?")
        builder1.setCancelable(true)
        builder1.setPositiveButton("Yes") { _, _->deleteRecord()}
        builder1.setNegativeButton("No") { dialog, _->dialog.cancel()}
        val alert11 = builder1.create()
        alert11.show()

    }

    /**
     * Metodo para reiniciar la vista de grabación luego de eliminar el audio grabado
     */
    private fun deleteRecord(){
        btnNextAudio.isEnabled=false
        audioRecord.visibility=View.GONE

    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(activity!!.applicationContext,
                WRITE_EXTERNAL_STORAGE)
        val result1 = ContextCompat.checkSelfPermission(activity!!.applicationContext,
                RECORD_AUDIO)
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED
    }


    private fun verifiyPermisied() {
        if (enablePermise(RECORD_AUDIO)|| enablePermise(WRITE_EXTERNAL_STORAGE) ||
                enablePermise(Manifest.permission.CAPTURE_AUDIO_OUTPUT) ) {
            if (Build.VERSION.SDK_INT >= 23) {
                ActivityCompat.requestPermissions(this.activity!!, arrayOf(RECORD_AUDIO, WRITE_EXTERNAL_STORAGE, Manifest.permission.CAPTURE_AUDIO_OUTPUT), REQUEST_PERMISSION_CODE)
            }
        }
    }


    private fun enablePermise(permission:String):Boolean{
        return ContextCompat.checkSelfPermission(this.context!!, permission) != PackageManager.PERMISSION_GRANTED
    }

    /**
     * Metodo para detener la grabacíón del audio del usuario
     */
    override fun finishRecord(nameAudio: String) {
        stopAnimation()
        audioRecord.visibility=View.VISIBLE
        val fileName=nameAudio.replace(Environment.getExternalStorageDirectory().absolutePath + "/game_catolic_quechua/","")
        val audioFile = File(nameAudio)
        Util.copyFileUsingStream(audioFile,context)
        mediaPlayerHolderForRecord?.loadMediaFromPath(nameAudio)
        lastAudioRecord=fileName

    }

    /**
     * Boton para reiniciar la vista luego de enviar un audio
     */
    private fun nextAudio(){
        btnNextAudio.isEnabled=false
        btnRecord.isEnabled=true
        index++
        tvAvance.text = "${index+1}/$totalAudio"
        textQuechua.setText(listTextAudio[index].text)
        val user=SessionManager.getInstance(context!!).userLogged
        user.avance=index
        SessionManager.getInstance(context!!).updateUserSession(user)
        audioRecord.visibility=View.GONE
        getTextPromp(user.idLanguage)
    }

    /**
     * Metodo para pasar al siguiente texto sin enviar el audio
     */
    private fun nextAudioWhitoutSend(){
        btnNextAudio.isEnabled=false
        btnRecord.isEnabled=true
        index++
        tvAvance.text = "${index+1}/$totalAudio"
        textQuechua.setText(listTextAudio[index].text)
        val user=SessionManager.getInstance(context!!).userLogged
        user.avance=index
        SessionManager.getInstance(context!!).updateUserSession(user)
        audioRecord.visibility=View.GONE

        getTextPromp(user.idLanguage)
    }

    /**
     * Metodo para enviar la grabación del audio al servidor
     */
    private fun updateServerRemote(){
        val progress = Util.createProgressDialog(context!!, "Cargando")
        progress.show()

        val file=File(context!!.filesDir, lastAudioRecord)
        val requestBody = RequestBody.create(null,file )
        RafiServiceWrapper.uploadAudio(lastAudioRecord,requestBody,context!!,
                {
                    getTextPromp(user.idLanguage)
                    progress.dismiss()
                }
                ,{
            error->
            progress.dismiss()
            Toast.makeText(context!!,error,Toast.LENGTH_LONG).show()
        })
        getTextPromp(user.idLanguage)
    }

    /**
     * Metodo para obtener el listado de textos de pruebas
     * @param idLanguage id del dialecto del cual se desean obtener textos de pruebas
     */
    private fun getTextPromp(idLanguage:Int){
        RafiServiceWrapper.getTextPromp(context!!, LangRequestDto(idLanguage),
                { list->
                    val gson = Gson()
                    val jsonString = gson.toJson(list)
                    Log.d("finishList",jsonString)
                    FileHelper.writeToFile(context!!,jsonString)
                },{
            error->
            Toast.makeText(context, error, Toast.LENGTH_LONG).show()
        })
    }

    /**
     * Metodo para iniciar la animación de audio
     */
    private fun initAnimation() {
        audioRecord.visibility=View.GONE
        btnPauseRecord.isEnabled=true
        cardAnimation.visibility=View.VISIBLE
        ivAnimation.visibility = View.VISIBLE
        ivAnimation.setBackgroundResource(R.drawable.animsound)
        frameAnimation = ivAnimation.background as AnimationDrawable
        frameAnimation?.start()
    }


    /**
     * Metodo para ocultar la animación de audio
     */
    private fun stopAnimation() {
        cardAnimation.visibility=View.GONE
        btnNextAudio.isEnabled=true
        btnPauseRecord.isEnabled=false
        frameAnimation?.stop()
        ivAnimation.setVisibility(View.GONE)
    }

}
