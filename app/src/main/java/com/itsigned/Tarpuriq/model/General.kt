package com.itsigned.Tarpuriq.model

data class RegisterUserDto(
        var email: String,
        var password:String,
        var phone:String,
        var distrito_id:String,
        var region_id:String,
        var last_name:String,
        var first_name:String,
        var provincia_id:String,
        var dni:String,
        var native_lang:Int
)

data class LoginUserDto(
        var count:Any ,
        var first_name: String,
        var last_name: String,
        var user_app_id: Int,
        var dni: Int,
        var email: String,
        var lang:Int
)

data class LoginRequestDto(
        var email:String,
        var password:String
)

data class UserByEmailRequestDto(
        var email:String
)

data class LangRequestDto(
        var lang:Int
)

data class  TextPrompDto(
        val text_prompts:List<TextAudio>
)

data class  ResponseLangDto(
        val language:List<Language>
)

data class Language(
        val iso:String,
        val language_id:Int,
        val name:String
){
    override fun toString(): String {
        return name
    }
}

data class TextAudio(
        val text:String,
        val id:Int
)

