package com.itsigned.Tarpuriq.helper

import android.content.Context
import android.util.Log
import java.io.*

/**
 * Clase con metodos utilitarios para trabajar con archivos
 */
class FileHelper {
    companion object{
        /**
         * Metodo para escribir el archivo de configuración
         */
        fun writeToFile(context: Context,data: String) {
            try {
                val outputStreamWriter = OutputStreamWriter(context.openFileOutput("config.txt", Context.MODE_PRIVATE))
                outputStreamWriter.write(data)
                outputStreamWriter.close()
            } catch (e: IOException) {
                Log.e("Exception", "File write failed: " + e.toString())
            }
        }

        /**
         * Metodo para leer los archivos de configuración
         */
         fun readFromFile(context:Context): String? {
            var ret = ""
            try {
                val inputStream: InputStream = context.openFileInput("config.txt")
                if (inputStream != null) {
                    val inputStreamReader = InputStreamReader(inputStream)
                    val bufferedReader = BufferedReader(inputStreamReader)
                    var receiveString: String? = ""
                    val stringBuilder = StringBuilder()
                    while (bufferedReader.readLine().also({ receiveString = it }) != null) {
                        stringBuilder.append(receiveString)
                    }
                    inputStream.close()
                    ret = stringBuilder.toString()
                }
            } catch (e: FileNotFoundException) {
                Log.e("login activity", "File not found: " + e.toString())
            } catch (e: IOException) {
                Log.e("login activity", "Can not read file: $e")
            }
            return ret
        }

    }
}