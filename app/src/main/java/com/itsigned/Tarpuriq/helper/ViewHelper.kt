package com.itsigned.Tarpuriq.helper

import android.content.Intent
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.itsigned.Tarpuriq.activity.MainActivity
import com.itsigned.Tarpuriq.util.Constants

fun AppCompatActivity.goToActivity(cl:Class<*> = MainActivity::class.java, finish:Boolean=true, clearAll:Boolean=false) {
    if(finish)this.finish()
    val mainIntent = Intent(this,cl )
    if(clearAll) mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
    startActivity(mainIntent)
}

/**
 * Metodo para mostrar un error en un editText de tipo editTextVAcio
 * @param editText edittext donde se mostrara el error
 * @param message mensaje de error a mostrar
 */
fun AppCompatActivity.hasErrorEditTextEmpty(editText: EditText, idMessage:Int):Boolean{
    if(editText.text.toString().compareTo(Constants.VACIO)==0){
        showError(editText, getString(idMessage))
        return true
    }
    return false
}

/**
 * Metodo para mostrar un error en un editText
 * @param editText edittext donde se mostrara el error
 * @param message mensaje de error a mostrar
 */
fun AppCompatActivity.showError(editText: EditText, message: String) {
    Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
    editText.error = message
    editText.isFocusable = true
    editText.requestFocus()
}